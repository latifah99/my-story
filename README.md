It is the collection of all my homework projects of the course Multimedia Technology and Applications in the first semester of my Master of Engineering program, which contains the implementations of some classical data compression algorithms, basic website technology, some desktop applications and so on.

Some brief introduction of every single project will be listed in the following part, but for more details, please check out the subfolders of the related projects, and there is a detail report describing what is the project and how to run it in every single subproject.**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

